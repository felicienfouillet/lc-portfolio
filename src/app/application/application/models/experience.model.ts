export interface Experience {
    id: number;

    title: string;

    subtitle: string;

    date: string;

    description: string;
}
