export interface Education {
    id: number;

    title: string;

    subtitle: string;

    date: string;

    description: string;
}
