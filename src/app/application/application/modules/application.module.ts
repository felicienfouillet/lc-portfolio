import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { ApplicationRoutingModule } from './application-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ApplicationRoutingModule
  ],
  providers: []
})
export class ApplicationModule { }
