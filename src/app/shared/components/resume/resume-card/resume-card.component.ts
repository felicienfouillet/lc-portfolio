import { animate, state, style, transition, trigger } from '@angular/animations';
import { Component, ElementRef, Input, OnInit } from '@angular/core';

@Component({
  selector: 'resume-card',
  templateUrl: './resume-card.component.html',
  styleUrls: ['./resume-card.component.scss'],
  animations: [
    trigger('onVisibleAnimation', [
      state('visible', style({
        opacity: 1,
        transform: 'translateY(0)'
      })),
      state('hidden', style({
        opacity: 0,
        transform: 'translateY(-50px)'
      })),
      transition('visible => hidden', [
        animate('.8s cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
      transition('hidden => visible', [
        animate('.8s cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
  ],
})
export class ResumeCardComponent implements OnInit {
  @Input()
  public title: string;

  @Input()
  public subtitle: string;

  @Input()
  public date: string;

  @Input()
  public description: string;

  public isAnimating: boolean;
  private defaults: any = {
    offset: -50
  };

  constructor(private element: ElementRef) { }

  ngOnInit(): void {
    this.animate();
  }

  public animate(): void {
    const isInView = this.isInViewport();

    if (!isInView) this.isAnimating = false;
    if (!isInView || this.isAnimating) return;

    this.isAnimating = true;
  }

  private isInViewport(): boolean {
    const bounding = this.element.nativeElement.getBoundingClientRect();

    const top = bounding.top - (window.innerHeight || document.documentElement.clientHeight);
    const bottom = bounding.top + bounding.height + this.defaults.offset;

    return top < 0 && bottom > 0;
  }
}
