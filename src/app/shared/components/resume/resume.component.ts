import { Component, EventEmitter, OnInit } from '@angular/core';
import { Education } from 'src/app/application/application/models/education.model';
import { Experience } from 'src/app/application/application/models/experience.model';

@Component({
  selector: 'resume',
  templateUrl: './resume.component.html',
  styleUrls: ['./resume.component.scss']
})
export class ResumeComponent implements OnInit {
  public trainings: Education[];
  public experiences: Experience[];

  constructor() { }

  ngOnInit(): void {
    this.trainings = [
      {
        id: 0,
        title: '1ère année de cycle ingénieur',
        subtitle: 'spécialité informatique',
        date: 'Depuis Septembre 2020',
        description: 'Cesi | Saint Nazaire'
      },
      {
        id: 1,
        title: 'Classe Préparatoire Intégrée',
        subtitle: 'spécialité informatique',
        date: 'Depuis Septembre 2020',
        description: 'Cesi | Saint Nazaire'
      },
      {
        id: 2,
        title: 'Certification Cisco - CCNA',
        subtitle: 'CCNA1 et CCNA2',
        date: '',
        description: ''
      },
      {
        id: 3,
        title: 'Baccalauréat S – option ISN',
        subtitle: 'Informatique et Sciences du Numérique',
        date: '2018',
        description: 'Lycée Alcide d’Orbigny | Bouaye'
      }
    ];

    this.experiences = [
      {
        id: 0,
        title: 'Stage bac +3 - Cycle ingénieur',
        subtitle: 'DevToBeCurious',
        date: 'Janvier 2021 à Avril 2021',
        description: 'CV gamifié : outil de suivi de formation, Développement web Angular et suivi de projet : Typescript, Javascript, HTML, CSS, C#, Gitlab, Trello'
      },
      {
        id: 1,
        title: 'Stage de seconde année de Classe Préparatoire Intégrée',
        subtitle: 'DevToBeCurious',
        date: 'Avril 2020 à Juillet 2020',
        description: 'Poulpix : Lead dev et gestion d’un projet de jeu vidéo mobile : C# et Javascript, Godot Engine'
      },
      {
        id: 2,
        title: 'La diagonale du poulpe',
        subtitle: 'Bénévole',
        date: 'Depuis Août 2020',
        description: 'Poulpix : Développement et gestion d’un jeu vidéo mobile. Synale Memories : Développement d’un jeu vidéo pédagogique'
      },
      {
        id: 3,
        title: 'Hôte de caisse',
        subtitle: 'Intermarché Le Pellerin',
        date: 'Avril 2017 à Avril 2020',
        description: 'CDI – Temps partiel'
      }
    ];
  }
}
