import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.scss']
})
export class NavBarComponent {

  isMobile: boolean = this.isMobileScreenSize();
  isItemsVisible: boolean = !this.isMobileScreenSize();

  constructor() { }

  @HostListener("window:resize")
  onResize() {
    this.isMobile = this.isMobileScreenSize();
    this.isItemsVisible = !this.isMobileScreenSize();
  }

  private isMobileScreenSize(): boolean {
    return window.screen.width <= 768 ? true : false;
  }
}
