import { animate, state, style, transition, trigger } from '@angular/animations';
import { AfterViewInit, Component, ElementRef, HostListener, OnInit } from '@angular/core';

@Component({
  selector: 'linear-divider',
  templateUrl: './linear-divider.component.html',
  styleUrls: ['./linear-divider.component.scss'],
  animations: [
    trigger('onScrollAnimation', [
      state('start', style({
        width: '10%'
      })),
      state('stop', style({
        width: '100%'
      })),
      transition('start => stop', [
        animate('.8s cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
      transition('stop => start', [
        animate('.8s cubic-bezier(0.35, 0, 0.25, 1)')
      ]),
    ]),
  ],
})
export class LinearDividerComponent implements AfterViewInit {

  isAnimating: boolean;
  top: number;
  initialTopDistance: number;

  constructor(private element: ElementRef) { }

  ngAfterViewInit(): void {
    this.initialTopDistance = (this.element.nativeElement as HTMLElement).getBoundingClientRect().top;
  }

  animate() {
    this.top = (this.element.nativeElement as HTMLElement).getBoundingClientRect().top;

    const isReady = this.top < this.initialTopDistance * .5;

    if (!isReady) this.isAnimating = false;
    if (!isReady || this.isAnimating) return;

    this.isAnimating = true;
  }
}
