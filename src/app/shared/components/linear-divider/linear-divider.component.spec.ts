import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LinearDividerComponent } from './linear-divider.component';

describe('LinearDividerComponent', () => {
  let component: LinearDividerComponent;
  let fixture: ComponentFixture<LinearDividerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LinearDividerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LinearDividerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
