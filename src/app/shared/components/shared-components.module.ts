import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from '../material.module';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { ResumeCardComponent } from './resume/resume-card/resume-card.component';
import { ResumeComponent } from './resume/resume.component';
import { LinearDividerComponent } from './linear-divider/linear-divider.component';



@NgModule({
  declarations: [
    NavBarComponent,
    ResumeComponent,
    ResumeCardComponent,
    LinearDividerComponent
  ],
  imports: [
    CommonModule,
    MaterialModule
  ],
  exports: [
    NavBarComponent,
    ResumeComponent,
    ResumeCardComponent,
    LinearDividerComponent
  ]
})
export class SharedComponentsModule { }
